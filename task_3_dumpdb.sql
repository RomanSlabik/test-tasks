CREATE DATABASE  IF NOT EXISTS `test_task` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `test_task`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: test_task
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.32-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `contact`
--

DROP TABLE IF EXISTS `contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contact`
--

LOCK TABLES `contact` WRITE;
/*!40000 ALTER TABLE `contact` DISABLE KEYS */;
INSERT INTO `contact` VALUES (1,'Roman','2019-08-11 00:02:00'),(2,'Andriy','2019-08-10 00:01:00'),(3,'Oleg','2019-08-11 23:58:00'),(4,'Petro','2019-08-13 10:53:42');
/*!40000 ALTER TABLE `contact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contact_field`
--

DROP TABLE IF EXISTS `contact_field`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact_field` (
  `contact_id` int(11) NOT NULL,
  `field_id` int(11) NOT NULL,
  KEY `contac_field_fk0_idx` (`contact_id`),
  KEY `contac_field_fk1_idx` (`field_id`,`contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contact_field`
--

LOCK TABLES `contact_field` WRITE;
/*!40000 ALTER TABLE `contact_field` DISABLE KEYS */;
INSERT INTO `contact_field` VALUES (1,1),(1,2),(1,3),(1,4);
/*!40000 ALTER TABLE `contact_field` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `custom_field`
--

DROP TABLE IF EXISTS `custom_field`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `custom_field` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `type_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `custom_field_fk0_idx` (`type_id`),
  CONSTRAINT `custom_field_fk0` FOREIGN KEY (`type_id`) REFERENCES `field_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `custom_field`
--

LOCK TABLES `custom_field` WRITE;
/*!40000 ALTER TABLE `custom_field` DISABLE KEYS */;
INSERT INTO `custom_field` VALUES (1,'phone number',1),(2,'update_at',2),(3,'sex',3),(4,'language',4);
/*!40000 ALTER TABLE `custom_field` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `date_value`
--

DROP TABLE IF EXISTS `date_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `date_value` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `custom_field_id` int(11) NOT NULL,
  `value` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `date_value_fk0_idx` (`custom_field_id`),
  CONSTRAINT `date_value_fk0` FOREIGN KEY (`custom_field_id`) REFERENCES `custom_field` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `date_value`
--

LOCK TABLES `date_value` WRITE;
/*!40000 ALTER TABLE `date_value` DISABLE KEYS */;
INSERT INTO `date_value` VALUES (1,2,'2019-08-10 17:44:41');
/*!40000 ALTER TABLE `date_value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_type`
--

DROP TABLE IF EXISTS `field_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_type`
--

LOCK TABLES `field_type` WRITE;
/*!40000 ALTER TABLE `field_type` DISABLE KEYS */;
INSERT INTO `field_type` VALUES (2,'date'),(4,'multiple select'),(3,'select'),(1,'text');
/*!40000 ALTER TABLE `field_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `select_option`
--

DROP TABLE IF EXISTS `select_option`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `select_option` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `custom_field_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `select_option_fk0_idx` (`custom_field_id`),
  CONSTRAINT `select_option_fk0` FOREIGN KEY (`custom_field_id`) REFERENCES `custom_field` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `select_option`
--

LOCK TABLES `select_option` WRITE;
/*!40000 ALTER TABLE `select_option` DISABLE KEYS */;
INSERT INTO `select_option` VALUES (1,3,'male'),(2,3,'female'),(3,4,'Ua'),(4,4,'UK'),(5,4,'USA');
/*!40000 ALTER TABLE `select_option` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `selecte_value`
--

DROP TABLE IF EXISTS `selecte_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `selecte_value` (
  `option_id` int(11) NOT NULL,
  `field_id` int(11) NOT NULL,
  KEY `selecte_value_fk0_idx` (`option_id`),
  KEY `selecte_value_fk1_idx` (`field_id`,`option_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `selecte_value`
--

LOCK TABLES `selecte_value` WRITE;
/*!40000 ALTER TABLE `selecte_value` DISABLE KEYS */;
INSERT INTO `selecte_value` VALUES (1,3),(3,4),(4,4),(5,4);
/*!40000 ALTER TABLE `selecte_value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `text_value`
--

DROP TABLE IF EXISTS `text_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `text_value` (
  `custom_field_id` int(11) NOT NULL,
  `value` text NOT NULL,
  KEY `text_value_fk0_idx` (`custom_field_id`),
  CONSTRAINT `text_value_fk0` FOREIGN KEY (`custom_field_id`) REFERENCES `custom_field` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `text_value`
--

LOCK TABLES `text_value` WRITE;
/*!40000 ALTER TABLE `text_value` DISABLE KEYS */;
INSERT INTO `text_value` VALUES (1,'+38050088546');
/*!40000 ALTER TABLE `text_value` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-08-13 17:46:41
